<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-window-restore"></i>
        </div>
        <div class="sidebar-brand-text mx-3">index 36 <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">



    <!-- QUERY MENU-->
    <?php
    $role_id = $this->session->userdata('role_id');
    $queryMenu = "SELECT  `user_menu` . `id`, `menu`       
                FROM      `user_menu` JOIN `user_access_menu`         
                ON        `user_menu` . `id` = `user_access_menu` . `menu_id`              
                WHERE     `user_access_menu` . `role_id` = $role_id       
                ORDER BY  `user_access_menu` . `menu_id` ASC
";
    $menu = $this->db->query($queryMenu)->result_array();
    var_dump($menu);
    die;

    ?>




    <!-- Heading -->
    <div class="sidebar-heading">
        administrator
    </div>
    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-solid fa-users"></i>
            <span>My Profile</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">


    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
            <i class="fas fa-fw fa-solid fa-arrow-right-from-bracket"></i>
            <span>Logout</span></a>
    </li>

    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">





    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->